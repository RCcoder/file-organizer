/* 
 * File:   parser.cpp
 * Author: victor
 *
 * Created on 15 de octubre de 2014, 16:25
 */

#include <cstdlib>
#include "includes/analyzer.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    char expstr[80];
    analyzer equation; // Instantiates equation as an object of type analyzer.
    for(;;)   // Creates an infinite loop.
    {
        cout << endl<< endl;
        cout << "  To stop the program" << endl;
        cout << "  enter a period then press return." << endl;
        cout << "  To run the program" << endl << endl;
        cout << "  enter an algebraic express; for example, (12+5)*6 " << endl;
        cout << "  then press return ==> ";
        cin.getline(expstr, 79);
        
        if(*expstr == '.')
        {
            equation.~analyzer();
            break;      // Infinite loop exited by typing a period.
        }
        cout << endl << endl << "  Answer is: " << equation.sniff(expstr) << "\n\n";
        cout << endl << endl << endl;
        cout << "  ";
        //system("PAUSE");
        //system("clear");
    };
    
    cout << endl << endl << endl;
    cout << "  ";
    system("PAUSE");
    
    return 0;
}

