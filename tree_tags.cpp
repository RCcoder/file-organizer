/* 
 * File:   tree_tags.cpp
 * Author: victor
 * 
 * Created on 23 de octubre de 2014, 14:01
 */

#include "tree_tags.h"

using namespace std;


tree_tags::tree_tags(): key_word_("TAGS") {
}

tree_tags::tree_tags(const tree_tags& orig) {
}

tree_tags::~tree_tags() {
}

tree_tags::addTags(ifstream file_pos_) {
    /*The first step is to read the tag from file
     and check if the tag is already created*/
    
    //LLAMADA A PARSER PARA LEER TAG
    
    //if(checkTag(file_pos_)){ PARECE QUE NO HAY 1 TAG CON VARIOS VALORES
    //SINO VARIOS TAGS IGUALES CON VARIOS VALORES
        /*Add more data to the same key
         which is actually the TAG*/
        
    //}
    //else{
        //no Tag is found
        list_.insert(pair<string, string> (tag_, date_));
    //}
}

void printTree(){
    string tag;
    //INFO next strategy is explain in http://www.cplusplus.com/reference/map/multimap/key_comp/
    multimap<string,string>::key_compare compar = list_.key_comp();
    for(multimap<string, string>::iterator it = list_.begin(); it != list_.end(); it++)
    {
        if(compar((*it).first, tag)){
            cout << " ---> "(*it).second << endl;
        }
        else
            cout << (*it).first << " ---> "(*it).second << endl;
        tag = (*it).first;
    }
}
