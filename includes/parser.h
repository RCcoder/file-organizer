/* 
 * File:   parser.h
 * Author: victor
 *
 * Created on 23 de octubre de 2014, 16:01
 */

#ifndef PARSER_H
#define	PARSER_H

class parser {
public:
    parser();
    parser(const parser& orig);
    virtual ~parser();
    
    searchDateTag();    //look for the next line with a date structure
    std::string nextTag();  //look for the next tag word in the line. Returns the word
    
private:

};

#endif	/* PARSER_H */

