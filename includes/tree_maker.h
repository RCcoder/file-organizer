/* 
 * File:   tree_maker.h
 * Author: victor
 *
 * This file defines a class that will be able to parse a file and
 * create a tree structure based on the different tags that it
 * will obtain from the file. It is a way to make indexes for
 * text files that I use in my dayly work in which I use TAGS to find
 * information when needed.
 * 
 * Created on 23 de octubre de 2014, 11:30
 */

#ifndef TREE_MAKER_H
#define	TREE_MAKER_H
#include <map>

using namespace std;

//struct multitag {
    /*TODO why using a structure? is it needed
     With a multimap seems enough to safe the different keys
     and values. Maybe to savo to files or something*/
    
//};

class tree_maker{
private:
    multimap<string key,string date> list;
    const std::string key_word_; //In the constructor, it receives a key word
    //that marks the start of the Tags for each entry
    //iftream scan_file;
    bool checkTag(ifstream file_pos_); //checks if the TAG is already created
    bool isThereTag(ifstream  file_pos_); //looks if there is TAG element to mark
                                //where are the real tag values
    void lookDate(ifstream  file_pos_);       //look for date structure and position in the file
public:
    void checkTags(ifstream file_pos_);    //to look and add new tags to the tree in list
    void printTree(void);       //print the keys and dates tree
    
};

#endif	/* TREE_MAKER_H */

