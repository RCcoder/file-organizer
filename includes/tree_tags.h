/* 
 * File:   tree_tags.h
 * Author: victor
 *
 * Created on 23 de octubre de 2014, 14:01
 */

#ifndef TREE_TAGS_H
#define	TREE_TAGS_H
#include <map>

using namespace std;

class tree_tags {
public:
    tree_tags();
    tree_tags(const tree_tags& orig);
    virtual ~tree_tags();
private:
    //TODO iterator for multimap
    //std::multimap<std::string key,std::string date>::iterator it_map_;
    multimap<string,string> list_;
    string tag_;        //will safe the read tag value each time
    string date_;       //will sage the read date value each time
    const string key_word_; //In the constructor, it receives a key word
    //that marks the start of the Tags for each entry
    //iftream scan_file;
    bool checkTag(ifstream file_pos_, multimap<string,string>::iterator& iterator); //checks if the TAG is already created
    bool isThereTag(ifstream  file_pos_); //looks if there is TAG element to mark
                                //where are the real tag values
    void lookDate(ifstream  file_pos_);       //look for date structure and position in the file
public:
    void addTags(ifstream file_pos_);    //to look and add new tags to the tree in list
    void printTree(void);       //print the keys and dates tree
    
};

#endif	/* TREE_TAGS_H */

