/* 
 * File:   analyzer.h
 * Author: victor
 *
 * Created on 15 de octubre de 2014, 16:32
 */

#ifndef ANALYZER_H
#define	ANALYZER_H

#include <cstring>
#include <cctype>
#include <cstdlib>
#include <iostream>


enum types { DELIMITER = 1, VARIABLE, NUMBER}; //DEL=1, VAR = 2, NUM = 3

class analyzer
{
private:
    char *pExpresion;   //Points to the expresion
    char token[80];     //Holds current token
    char tokenType;     //Holds token's type
    
    void recursive1(double &culmination);
    void recursive2(double &culmination);
    void recursive3(double &culmination);
    void recursive4(double &culmination);
    void recursive5(double &culmination);
    void bomb(double &culmination);
    void nextToken();
    void serror(int error);
    int isdelim(char c);
    
public:
    analyzer();                 //constructor
    double sniff(char *exp);    //analyzer entry point
    ~analyzer();
};


#endif	/* ANALYZER_H */

